#pragma once
#include <string>
#include <vector>

#include "Mesh.h"
#include "ModelMaterial.h"
class Model
{
public:
	Model(const std::string a_FileName);
	~Model();

	bool HasMeshes() const; 
	bool HasMaterials() const;

	const std::vector<Mesh*>& Meshes() const; 
	const std::vector<ModelMaterial*>& Materials() const;

private:
	Model(const Model& rhs);
	Model& operator=(const Model& rhs);

	std::vector<Mesh*> mMeshes;
	std::vector<ModelMaterial*> mMaterials;
};

