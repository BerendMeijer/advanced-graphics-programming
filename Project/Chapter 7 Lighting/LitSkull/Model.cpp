#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <Windows.h>
#include "Mesh.h"

#include "Model.h"



Model::Model(const std::string a_FileName)
	: mMeshes(), mMaterials()
{
	Assimp::Importer importer;
	UINT flags = aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType; 

	const aiScene* scene = importer.ReadFile(a_FileName, flags);
	if (scene == nullptr)
	{
		throw std::exception("Couldn't find the file");
	}

	if (scene->HasMaterials())
	{
		for (UINT i = 0; i < scene->mNumMaterials; i++)
		{
			mMaterials.push_back(new ModelMaterial(*this, scene->mMaterials[i]));
		}
	}

	if (scene->HasMeshes())
	{
		for (UINT i = 0; i < scene->mNumMeshes; i++)
		{
			Mesh* mesh = new Mesh(*this, *(scene->mMeshes[i]));
			mMeshes.push_back(mesh);
		}
	}
}

Model::~Model()
{
	for (Mesh* mesh : mMeshes)
	{
		delete mesh;
	}

	for (ModelMaterial* material : mMaterials)
	{
		delete material;
	}
}

bool Model::HasMeshes() const
{
	return(mMeshes.size() > 0);
}

bool Model::HasMaterials() const
{
	return (mMaterials.size() > 0);
}

const std::vector<Mesh*>& Model::Meshes() const
{
	return mMeshes;
}

const std::vector<ModelMaterial*>& Model::Materials() const
{
	return mMaterials;
}
