#include "d3dApp.h"
#include "d3dx11Effect.h"
#include "GeometryGenerator.h"
#include "MathHelper.h"
#include "LightHelper.h"
#include "Effects.h"
#include "Vertex.h"
#include "Camera.h"
#include "Sky.h"
#include "RenderStates.h"
#include "ShadowMap.h"
#include "Ssao.h"
#include "TextureMgr.h"
#include "BasicModel.h"
#include "Model.h"

DirectionalLight DirLights[3];
Material Mat;

Model::Model()
{
	DirLights[0].Ambient = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	DirLights[0].Diffuse = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	DirLights[0].Specular = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	DirLights[0].Direction = XMFLOAT3(0.0f, 0.0f, 0.0f);

	Mat.Ambient = XMFLOAT4(0.48f, 0.77f, 0.46f, 1.0f);
	Mat.Diffuse = XMFLOAT4(0.48f, 0.77f, 0.46f, 1.0f);
	Mat.Specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 16.0f);
}

Model::~Model()
{
	ReleaseCOM(mModel.VertexBuffer);
	ReleaseCOM(mModel.IndexBuffer);
}


void Model::LoadModel(const std::string& filename, ID3D11Device* pDevice)
{
	Assimp::Importer imp;

	UINT NumMeshes = 0;
	UINT NumFaces = 0;

	const aiScene* pScene = imp.ReadFile(filename,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_GenSmoothNormals |
		aiProcess_SplitLargeMeshes |
		aiProcess_ConvertToLeftHanded |
		aiProcess_SortByPType |
		aiProcess_PreTransformVertices);

	if (pScene == NULL)
		//ShowError(imp.GetErrorString());

	NumMeshes = pScene->mNumMeshes;

	std::vector<Vertex::Basic32> vertices;
	std::vector<DWORD> indices;

	for (UINT i = 0; i < NumMeshes; i++)
	{
		aiMesh* mesh = pScene->mMeshes[i];

		for (UINT j = 0; j < mesh->mNumVertices; j++)
		{
			Vertex::Basic32 v;

			v.Pos.x = mesh->mVertices[j].x;
			v.Pos.y = mesh->mVertices[j].y;
			v.Pos.z = mesh->mVertices[j].z;

			v.Normal.x = mesh->mNormals[j].x;
			v.Normal.y = mesh->mNormals[j].y;
			v.Normal.z = mesh->mNormals[j].z;

			if (mesh->HasTextureCoords(0))
			{
				v.Tex.x = mesh->mTextureCoords[0][j].x;
				v.Tex.y = mesh->mTextureCoords[0][j].y;
			}

			vertices.push_back(v);
		}

		for (UINT c = 0; c < mesh->mNumFaces; c++)
			for (UINT e = 0; e < mesh->mFaces[c].mNumIndices; e++)
				indices.push_back(mesh->mFaces[c].mIndices[e]);

	}


	mModel.mNumVertices = vertices.size();
	mModel.mNumFaces = indices.size() / 3;

	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(Vertex::Basic32) * vertices.size();
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vinitData;
	vinitData.pSysMem = &vertices[0];

	HR(pDevice->CreateBuffer(&vbd, &vinitData, &mModel.VertexBuffer));

	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(DWORD) * indices.size();
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA iinitData;
	iinitData.pSysMem = &indices[0];

	HR(pDevice->CreateBuffer(&ibd, &iinitData, &mModel.IndexBuffer));

}


void Model::Render(CXMMATRIX world, Camera mCam, ID3D11DeviceContext* pDeviceContext)
{
	UINT Stride = sizeof(Vertex::Basic32);
	UINT Offset = 0;

	Effects::BasicFX->SetDirLights(DirLights);
	Effects::BasicFX->SetEyePosW(mCam.GetPosition());

	ID3DX11EffectTechnique* activeTech = Effects::BasicFX->Light3Tech;

	D3DX11_TECHNIQUE_DESC techDesc;
	activeTech->GetDesc(&techDesc);

	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		pDeviceContext->IASetVertexBuffers(0, 1, &mModel.VertexBuffer, &Stride, &Offset);
		pDeviceContext->IASetIndexBuffer(mModel.IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		XMMATRIX worldInvTranspose = MathHelper::InverseTranspose(world);
		XMMATRIX worldViewProj = world * mCam.ViewProj();

		Effects::BasicFX->SetWorld(world);
		Effects::BasicFX->SetWorldInvTranspose(worldInvTranspose);
		Effects::BasicFX->SetWorldViewProj(worldViewProj);
		Effects::BasicFX->SetMaterial(Mat);

		activeTech->GetPassByIndex(p)->Apply(0, pDeviceContext);
		pDeviceContext->DrawIndexed(mModel.mNumFaces * 3, 0, 0);
	}

}