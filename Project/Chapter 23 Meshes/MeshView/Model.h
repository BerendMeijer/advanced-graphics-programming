#ifndef _MODEL_H_
#define _MODEL_H_

#include "d3dApp.h"
#include "d3dx11Effect.h"
#include "MathHelper.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Model
{
public:
	Model();
	~Model();

	void LoadModel(const std::string& filename, ID3D11Device* pDevice);
	void Render(CXMMATRIX world, Camera mCam, ID3D11DeviceContext* pDeviceContext);

private:
	struct ModelData
	{
		ModelData()
		{
			VertexBuffer = nullptr;
			IndexBuffer = nullptr;
		}
		ID3D11Buffer* VertexBuffer;
		ID3D11Buffer* IndexBuffer;

		UINT mNumMeshes;
		UINT mNumVertices;
		UINT mNumFaces;
	};


	ModelData mModel;
};

#endif
